/**
 * Created by lichong on 7/14/2017.
 */
import React from 'react';
import App from './App';
import {Route, BrowserRouter, Switch} from 'react-router-dom';
import CategoryCreatePage from './pages/category/create';
import CategoryListPage from './pages/category/list';
import AttrCreatePage from './pages/attribute/create';
import AttrEditPage from './pages/attribute/edit';
import AttrListPage from './pages/attribute/list';
import SpecCreatePage from './pages/specification/create';
import SpecEditPage from './pages/specification/edit';
import SpecListPage from './pages/specification/list'
import BrandCreatePage from './pages/brand/create';
import BrandEditPage from './pages/brand/edit';
import BrandListPage from './pages/brand/list';
import ProductListPage from './pages/product/list';
import ProductCreatePage from './pages/product/create';
import ProductEditPage from './pages/product/edit';
import OrderListPage from './pages/order/list';
import SystemSettingPage from './pages/system/settings';
import ProductPromotionCreatePage from './pages/promotion/product/create';
import OrderPromotionCreatePage from './pages/promotion/order/create';
import CouponCreatePage from './pages/coupon/create';
import CouponListPage from './pages/coupon/list';
import SeckillCreatePage from './pages/seckill/create';
import UserListPage from './pages/user/list'
import StatisticsPage from './pages/statistics'
import DashboardPage from './pages/dashboard'
import LoginPage from './pages/passport/login'
import './index.css';

const routers =
    <BrowserRouter>
        <Switch>
            <Route path='/login' component={LoginPage} exact/>
            <Route path="/"
                   component={(props) => 
                        <App {...props}>
                            <Switch>
                                <Route path='/dashboard' component={DashboardPage}/>
                                <Route path='/products' component={ProductListPage}/>
                                <Route path='/product/create' component={ProductCreatePage} exact/>
                                <Route path='/product/:id' component={ProductEditPage} exact/>
                                <Route path='/orders' component={OrderListPage}/>
                                <Route path='/categories' component={CategoryListPage}/>
                                <Route path='/category/create' component={CategoryCreatePage} exact/>
                                <Route path='/attributes' component={AttrListPage}/>
                                <Route path='/attribute/create' component={AttrCreatePage} exact/>
                                <Route path='/attribute/:id' component={AttrEditPage} exact/>
                                <Route path='/specs' component={SpecListPage}/>
                                <Route path='/spec/create' component={SpecCreatePage} exact/>
                                <Route path='/spec/:id' component={SpecEditPage} exact/>
                                <Route path='/brands' component={BrandListPage}/>
                                <Route path='/brand/create' component={BrandCreatePage} exact/>
                                <Route path='/brand/:id' component={BrandEditPage} exact/>
                                <Route path='/promotion/product' component={ProductPromotionCreatePage}/>
                                <Route path='/promotion/order' component={OrderPromotionCreatePage}/>
                                <Route path='/promotion/seckill' component={SeckillCreatePage}/>
                                <Route path='/coupons' component={CouponListPage}/>
                                <Route path='/coupon/create' component={CouponCreatePage} exact/>
                                <Route path='/users' component={UserListPage}/>
                                <Route path='/system/settings' component={SystemSettingPage}/>
                                <Route path='/statistics' components={StatisticsPage}/>
                                <Route path='/404' component={App}/>
                            </Switch>
                        </App>
                   }
            >
            </Route>
        </Switch>
    </BrowserRouter>

export default routers;