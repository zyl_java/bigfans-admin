import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import AppHelper from 'utils/AppHelper'
import AttrSearchForm from './SearchForm'
import AttrSearchList from './SearchList'

class AttributeListPage extends React.Component {

	components = {}

	state = {
	    data: [],
	    pagination: {},
	    loading: false,
	    selectedRowKeys : []
	};

	constructor(props) {
		super(props);
	}

	addComponents(name , component){
		this.components[name] = component
	}

	changeLoading(loading) {
		this.setState({loading})
	}

	receiveData (data) {
		this.setState({data});
		this.setState({loading : false})
	}

	deleteRecord(record){
		let searchForm = this.components['searchForm']
		searchForm.reload()
	}

	render () {
		return (
			<div id="App-product-list">
				<Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>属性管理</Breadcrumb.Item>
                    <Breadcrumb.Item>属性列表</Breadcrumb.Item>
                </Breadcrumb>
				<Card bordered={false}>
					<AttrSearchForm 
						changeLoading={(e) => this.changeLoading(e)} 
						receiveData={(e) => this.receiveData(e)} 
						setSearchForm={(e)=>this.addComponents('searchForm' , e)}
					/>
					<AttrSearchList 
						dataSource={this.state.data} 
						loading={this.state.loading} 
						setSearchList={(e)=>this.addComponents('searchList' ,e)}
						onDeleteAttr={(e) => this.deleteRecord(e)}
					/>
				</Card>
			</div>
			)
	}
}

export default AttributeListPage;